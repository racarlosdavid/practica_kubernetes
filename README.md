# Comandos Practica kubernetes

## Configurar Runner de gitlab

`curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash`

`sudo apt-get install gitlab-runner`

`sudo gitlab-runner register`

## Instalacion nodejs y npm en el runner

`sudo apt install nodejs`

`sudo apt install npm`

## Instalacion docker y docker-compose en el runner

`git clone https://github.com/racarlosdavid/Scripts.git`

`cd Scripts/docker`

`chmod 777 install_docker.sh`

`./install_docker.sh`

`chmod 777 install_docker_compose.sh`

`./install_docker_compose.sh`

`sudo chmod 666 /var/run/docker.sock`

## Instalacion kubectl en el runner

`curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"`

`curl -LO "https://dl.k8s.io/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256"`

`echo "$(<kubectl.sha256) kubectl" | sha256sum --check`

`sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl`

## Creacion del cluster de Kubernetes

`gcloud config get-value project`

`gcloud config set project`

`gcloud config set compute/zone us-central1-a`

`gcloud container clusters create cluster-ayd2-practica --num-nodes=1 --tags=all-in,all-out --machine-type=n1-standard-2 --no-enable-network-policy`

## Obtener valores para linkear el cluster a gitlab

`kubectl cluster-info | grep -E 'Kubernetes master|Kubernetes control plane' | awk '/http/ {print $NF}'`

`kubectl get secrets`

`kubectl get secret <secret> -o jsonpath="{['data']['ca\.crt']}" | base64 --decode`

`kubectl apply -f gitlab-admin-service-account.yaml`

`kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab | awk '{print $1}')`


## Referencias
https://docs.gitlab.com/runner/install/linux-repository.html
https://docs.docker.com/engine/install/ubuntu/
https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/
